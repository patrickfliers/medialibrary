<?php 
	
	/**
	
	---------------------------------------------
	TODO: Restructure the code (clean up),
	Implement the PDO Connection in database.php,
	Include database.php and make an connection
	---------------------------------------------
		
	*/
	
	class Data {
				
		private $pdo;
		
		//Setting up an PDO Connection ones
		public function getPDO() {
			// Data for the PDO Connection
			$servername = 'localhost';
			$username = 'root';
			$password = 'root';
			$database = 'media_library';
			
			// Checks if an connection already exists.
			if($this->pdo === null) {
				// Try to connect to the database
				try {
					$conn = new PDO("mysql:host=$servername;dbname=$database", $username, $password);
				    // set the PDO error mode to exception
				    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
					// Setting the private pdo variable to the connection
					$this->pdo = $conn;
				}
				// When failed it'll show an error
				catch(PDOException $e) {
				    echo "Connection failed: " . $e->getMessage();
				    die();
				}
			}
			
			return $this->pdo;
		}

		public function getData() {
			
			// Creating an empty array for the data
			$mediaArray = array();
			
			// Define the query for getting the books:
			$query = 'SELECT * FROM book';		
			// Execute the query for books:
			$mediaArray['book'] = $this->getPDO()->query($query) ? $this->parser($this->getPDO()->query($query)) : array();
			
			// Define the query for getting the movie:
			$query = 'SELECT * FROM movie';
			// Execute the query for getting movies:
			$mediaArray['movie'] = $this->getPDO()->query($query) ? $this->parser($this->getPDO()->query($query)) : array();
			
			// Define the query for getting the music:
			$query = 'SELECT * FROM music';
			// Execute the query for getting music:
			$mediaArray['music'] = $this->getPDO()->query($query) ? $this->parser($this->getPDO()->query($query)) : array();
			
			// Returning the data from the database
			return $mediaArray;	
		}
		
		// Get data from specified 'category' and 'id' from the database
		public function getSingleItemFor($category, $id) {
			
			// Query selects one element on specified category and id
			$query = 'SELECT * FROM ' . $category . ' WHERE id = ' . $id . ' LIMIT 1';
			
			// Running the query and passing the data in the array
			$library = $this->getPDO()->query($query) ? $this->parser($this->getPDO()->query($query)) : array();
			
			foreach($library as $data) {
				return $data;
			}
			
		}
		
		
		// Function that will loop through the data of the database
		public function parser($object) {
			
			// Define the result array
			$result = [];
			// Looping through the data
			foreach($object as $value) {
				// Assign the data to the result array
				$result[] = $value;
			}
			// Returning the data
			return $result;
		}
	}