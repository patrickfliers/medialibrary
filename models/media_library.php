<?php
	
	class Library {
		
		// Function that will get all the data
		public function getMedia() {
			
			// Set an empty array for the media
			$mediaArray = array();
		
			// Filling the array with specific key and value(s)
			$mediaArray['book'] = array('A Design Patterns: Elements of Reusable Object-Oriented Software');
			$mediaArray['movie'] = array('Forrest Gump');
			$mediaArray['music'] = array('Beethoven: Complete Symphonies');
			
			// Checks if the array has data
			if(count($mediaArray) > 0) {
				
				// Set an empty array for the output
				$result = array();
							
				// Looping through the array to get the key
				$categoryKeys = array_keys($mediaArray);
				
				// Looping through the keys
				foreach($categoryKeys as $categoryKey) {
										
					// Looping multiple times through the array with same categoryKey
					foreach($mediaArray[$categoryKey] as $library) {
						
						$result[] = ucfirst($categoryKey) . ' called: ' . '\'' . ucwords($library) . '\'';
					}
				}
			} else {
				$result[] = 'Something went wrong, you\'re not giving us an array with data back.';
			}
			
			return implode('</br>', $result);
		}
	}
	
	
				