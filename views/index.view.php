	<?php 

		$pageTitle = 'Books';
		$section = null;
		
		// Import the data from the Data Class
		require_once('controllers/dataController.php');
		
		// Creating an object of the class Data
		$method = new Method();
		
		// Importing the header view
		require_once('views/partials/header.view.php');
	?>
	
		<div class="section catalog random">

			<div class="wrapper">

				<h2>May we suggest something?</h2>				

				<ul class="items">
					<!-- Displaying list with items using dataController.php class -->					
					<?php $categoryKeys = array_keys($method->getData()); ?>
					
					<?php foreach($categoryKeys as $categoryKey) { ?>
						<h1><?=ucfirst($categoryKey)?></h1>
						<?php foreach($method->getDataByCategory($categoryKey) as $data) { ?>
							<li>
								<a href="/views/detail.view.php?cat=<?=$categoryKey?>&id=<?=$data['id']?>">
									<img src="<?=$data['image_path']?>" alt="<?=$data['title']?>" />
									<p>View details</p>										
								</a>
							</li>
						<?php } ?>
					<?php } ?>
				</ul>
				

			</div>

		</div>
		
	<?php 
		// Importing the footer view
		require_once('views/partials/footer.view.php');
	?>