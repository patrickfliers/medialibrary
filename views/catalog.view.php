	<?php 
	 	
	 	// Importing the data model with key and values
		require_once('../controllers/dataController.php');
		
		// Creating an object of MediaLibrary class
		$method = new Method();
		$data = new Data();
			
		// Setting the title for this page
		$pageTitle = 'Full Catalog';
		$section = null;
		
		if (isset($_GET['cat'])) {
			if ($_GET['cat'] == 'book') {
				$pageTitle = 'Books';
				$section = 'book';
			} else if ($_GET['cat'] == 'movie') {
				$pageTitle = 'Movies';
				$section = 'movie';
			} else if ($_GET['cat'] == 'music') {
				$pageTitle = 'Music';
				$section = 'music';
			}
		}
		
		// Import the header view
		require_once('../views/partials/header.view.php'); 
	?>
	
		<div class="section catalog page">
			<div class="wrapper">
				<ul class="items">	
					<!-- Displaying list with items using dataController.php class -->	
					<h1><?=$pageTitle?></h1>
					<?php foreach($method->getDataByCategory($_GET['cat']) as $library) { ?>
					<li>
						<a href="detail.view.php?cat=<?=$_GET['cat']?>&id=<?=$library['id']?>">
							<img src="../<?=$library['image_path']?>" alt="<?=$library['title']?>" />
							<p>View detail</p>
						</a>
					</li>
					<?php } ?>
				</ul>
			</div>
		</div>
	
	
	<?php 
		// Import the footer template
		require_once('../views/partials/footer.view.php'); 
	?>
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	