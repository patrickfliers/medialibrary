	<?php 
		
		
		/**
			------------------------------------------------------------------			
			 TODO: 
			 1. genre, format and year are in id format, not with data yet
			 2. implement breadcrumbs on a better way then TTH
			------------------------------------------------------------------
		*/
		
		// Setting the title for this page
		$pageTitle = 'Detail';
		$section = 'detail';
		
		// Import the header template
		require_once('../views/partials/header.view.php'); 
		require_once('../models/data.php');		 
		
		$data = new Data();
		
		$mediaLibrary = array();
		
		if (!isset($_GET['id']) || !isset($_GET['cat'])) {
			header('location:catalog.view.php');
		} else {
			$mediaLibrary[] = $data->getSingleItemFor($_GET['cat'], $_GET['id']);	
		}
	?>
	
		<div class="section page">
			<div class="media-picture">
				<?php foreach($mediaLibrary as $item) { ?>
				<span><img src="../<?=$item['image_path']?>" alt="<?=$item['title']?>" /></span>
				<?php } ?>
			</div>
			
			<div class="media-details">
				<?php foreach($mediaLibrary as $item) { var_dump($item);?>
					<h1><?=$item['title']?></h1>
					<table>
						<tr>
							<th>Category</th>
							<td><?=ucfirst($_GET['cat'])?></td>
						</tr>
					</table>
					
				<?php } ?>
			</div>
		</div>
	
	
	<?php 
		
		// Import the footer template
		require_once('../views/partials/footer.view.php'); 
	?>
	