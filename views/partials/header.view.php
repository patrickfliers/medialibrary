<!DOCTYPE html>
<html>
<head>
	<title><?=$pageTitle?> | medialibrary</title>
	<link rel="stylesheet" href=" /public/css/style.css" type="text/css">
</head>
<body>

	<div class="header">

		<div class="wrapper">

			<h1 class="branding-title"><a href="/">Personal Media Library</a></h1>

			<ul class="nav">
				<?php if ($section == null) { ?>
	                <li class="books <?=$section == 'book' ?: ' on'?>"><a href="views/catalog.view.php?cat=book">Books</a></li>
	                <li class="movies <?=$section == 'movie' ?: ' on'?>"><a href="views/catalog.view.php?cat=movie">Movies</a></li>
	                <li class="music <?=$section == 'music' ?: ' on'?>?>"><a href="views/catalog.view.php?cat=music">Music</a></li>
	                <li class="suggest  <?=$section == 'suggest' ?: ' on'?>"><a href="views/suggest.view.php">Suggest</a></li>
                <?php } else { ?>
	                <li class="books <?=$section == 'book' ?: ' on'?>"><a href="catalog.view.php?cat=book">Books</a></li>
	                <li class="movies <?=$section == 'movie' ?: ' on'?>"><a href="catalog.view.php?cat=movie">Movies</a></li>
	                <li class="music <?=$section == 'music' ?: ' on'?>?>"><a href="catalog.view.php?cat=music">Music</a></li>
	                <li class="suggest  <?=$section == 'suggest' ?: ' on'?>"><a href="suggest.view.php">Suggest</a></li>
                <?php } ?>
            </ul>

		</div>

	</div>

	<div id="content">