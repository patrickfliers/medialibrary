<?php
	
	/* 
	---------------------------------------------------------------
    TODO: Implement PDO connection into here and make all the data
    request in the METHOD class to retreive data from the database
	---------------------------------------------------------------
	*/

	// Importing the data to use it
	require_once($_SERVER['DOCUMENT_ROOT'] . '/models/data.php');

	class Method {
		
		// Get data out of the Data Class and returns an associative array with Category as extra keys
		public function getData() {
						
			// Creating a new Data object
			$data = new Data();
			
			// Result array for the output
			$result = array();
			
			// Array assigning the data
			$mediaArray = $data->getData();
			
			// Define the array keys
			$mediaArrayKeys = array_keys($mediaArray);
			
			// Loop trough all the keys
			foreach($mediaArrayKeys as $mediaArrayKey ) {
				
				// Assigning the key in an associative array based if the array is set or not
				$result[$mediaArrayKey] = $mediaArray[$mediaArrayKey] > 0 ? $mediaArray[$mediaArrayKey] : array();
			}	
			
			// Returning an array with data
			return $result;
		}
		
		
		// Function that will retreive data based on the given 'Category Key'
		public function getDataByCategory($category) {
			
			// Result array for the output
			$result = array();
			
			// Getting the data
			$mediaArray = $this->getData();
						
			// Looping through the data per category (key)
			foreach($mediaArray[$category] as $media) {
				
				// Assigning array to output array
				$result[] = $media;
			}
			
			asort($result);
			
			// Returning an array with data
			return $result;
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
