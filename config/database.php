<?php
	
	/**
		---------------------------------------------------------------------
		With this file I make a single connection to the database using PDO.
		When you call the funtion in another file it will retreive 
		all the data using query's using the data class
		---------------------------------------------------------------------
	*/
	
	class Connection {
		
		private $pdo;
		
		public __construct($pdo) {
			$this->pdo = $pdo;
		}
		
		/** 
			Setting up an PDO Connection ones
		*/
		public function makeConnection() {
			// Default values for the PDO Connection
			$servername = 'localhost';
			$username = 'root';
			$password = 'root';
			$database = 'media_library';
			
			// Checks if an connection already exists.
			if($this->pdo === null) {
				// Try to connect to the database
				try {
					$conn = new PDO("mysql:host=$servername;dbname=$database", $username, $password);
				    // set the PDO error mode to exception
				    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
					// Setting the private pdo variable to the connection
					$this->pdo = $conn;
				}
				// When failed it'll show an error
				catch(PDOException $e) {
				    echo "Connection failed: " . $e->getMessage();
				    die();
				}
			}
			
			// Returns the connection or error
			return $this->pdo;
		}
	}
	
	
	
	
	
	